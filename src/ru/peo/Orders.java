package ru.peo;

import java.util.Arrays;

public class Orders {
        public static void main(String[] args) {
        int[] price = {11, 2};
        int t = 4;
        Arrays.sort(price);//сортируем массив
        System.out.println(Arrays.toString(price));//преобразовываем строку с помощью toString и выводим на экран
        int[] expensive = countOf(price, t);//результат работы метода (т.е. значение, которое он вернет),записываем в переменную
        int max_earned = 0;
        for (int i = 0; i < expensive.length; i++) {//нам в параметры пришло количество времени
                                                    //если оно меньше количества заказов - то берем его
                                                    //если больше - то берем количество заказов
            max_earned += expensive[i];
        }
        System.out.println("Максимальный заработок за "+t+", составит= " + max_earned);
    }

    private static int[] countOf(int[] array, int res) {//этот метод возвращает массив, содержащий стоимости самых выгодных заказов. 
//Длина этого массива - число возможных выполнений заказов

        int[] expensive = new int[res];
        if (res > array.length) {//если t больше количества заказов (т.е. длины массива price), то мы берем меньшее число
            res = array.length;
        }
        for (int i = 0; i < res; i++) {//берем значения массива с конца(уже отсортированного)
            expensive[i] = array[array.length - i - 1];//записываем значения массива в массив expensive.
//Мы элементу expensive присваиваем элемент из array (с конца)

            //в итоге наш массив expensive имеет длину - число заказов, которое мы МОЖЕМ выполнить
            //а значения - это самые выгодные стоимости заказов
            //и записываем их в массив expensive
        }
        return expensive;
    }
}
